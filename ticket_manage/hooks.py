# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version
from frappe import _

app_name = "ticket_manage"
app_title = "Ticket Manage"
app_publisher = "Mainul Islam"
app_description = "Ticket"
app_icon = "fa fa-device"
app_color = "green"
app_email = "mainulkhan94@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# fixtures = ['Custom Field', 'Property Setter']

# include js, css files in header of desk.html
app_include_css = "/assets/css/ticket_manage.css"
app_include_js = "/assets/js/ticket_manage.js"

# include js, css files in header of web template
# web_include_css = "/assets/ticket_manage/css/ticket_manage.css"
web_include_js = "/assets/js/ticket_manage.web.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
doctype_js = {
	"Sales Invoice": "public/js/doctype/sales_invoice.js"
	}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "ticket_manage.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "ticket_manage.install.before_install"
# after_install = "ticket_manage.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "ticket_manage.notifications.get_notification_config"
website_route_rules = [
	{"from_route": "/tickets", "to_route": "Ticket"},
	{"from_route": "/tickets/<path:name>", "to_route": "order",
		"defaults": {
			"doctype": "Ticket",
			"parents": [{"label": _("Ticket"), "route": "tickets"}]
		}
	}
]


standard_portal_menu_items = [
	{"title": _("Ticket"), "route": "/tickets", "reference_doctype": "Ticket", "role": "Sales User"}
]
# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
	"Sales Invoice": {
		"on_update": "ticket_manage.utils.sales_invoice.on_update",
		"validate": "ticket_manage.utils.sales_invoice.validate"
	}
}

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"ticket_manage.tasks.all"
# 	],
# 	"daily": [
# 		"ticket_manage.tasks.daily"
# 	],
# 	"hourly": [
# 		"ticket_manage.tasks.hourly"
# 	],
# 	"weekly": [
# 		"ticket_manage.tasks.weekly"
# 	]
# 	"monthly": [
# 		"ticket_manage.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "ticket_manage.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "ticket_manage.event.get_events"
# }

website_context = {
	"favicon": '/assets/ticket_manage/images/mini_splash.png',
	"splash_image": '/assets/ticket_manage/images/Splash.png'
}
