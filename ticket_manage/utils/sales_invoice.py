import frappe
from frappe.model.mapper import get_mapped_doc


def validate(doc, method):
    if not doc.flags.from_doctype:
        doc.flags.from_doctype = doc.doctype

def on_update(doc, method):
    if doc.devices and doc.flags.from_doctype == doc.doctype:
        sync_ticket(doc)

@frappe.whitelist()
def sync_ticket(doc):
    if doc.ticket:
        ticket = frappe.get_doc('Ticket', doc.ticket)
    else:
        ticket = frappe.new_doc('Ticket')
    ticket.flags.from_doctype = doc.doctype
    ticket = make_ticket(doc.name, ticket)
    ticket.save()
    doc.set('ticket', ticket.name)


@frappe.whitelist()
def make_ticket(source_name, target_doc=None):
	if target_doc:
		target_doc.set('devices', [])
		target_doc.set('items', [])
		target_doc.set('taxes', [])
	def postprocess(source, target):
		if not target.due_date:
			target.set('due_date', source.due_date)
	target_doc = get_mapped_doc("Sales Invoice", source_name,
		{
			"Sales Invoice": {
				"doctype": "Ticket",
			},
			"Sales Invoice Item": {
				"doctype": "Ticket Item",
				# "field_map":[
				# 	['ticket_details', 'name'],
				# 	['ticlet', 'parent']
				# ]
			}
		}, target_doc, postprocess)
	return target_doc
