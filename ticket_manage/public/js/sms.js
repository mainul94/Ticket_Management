frappe.provide('ticket')

ticket.SMSManager = function SMSM(doc) {
    $.extend(this, erpnext.SMSManager.apply(this, arguments))
    this.make_dialog = function() {
		var me = this;
		var d = new frappe.ui.Dialog({
			title: 'Send SMS',
			width: 400,
			fields: [
				{fieldname:'number', fieldtype:'Data', label:'Mobile Number', reqd:1},
				{fieldname:'sms_template', fieldtype:'Link', label:'Template', reqd:0, options: "SMS Template"},
				{fieldname:'message', fieldtype:'Text', label:'Message', reqd:1},
				{fieldname:'send', fieldtype:'Button', label:'Send'}
			]
		})
		d.fields_dict.send.input.onclick = function() {
			var btn = d.fields_dict.send.input;
			var v = me.dialog.get_values();
			if(v) {
				$(btn).set_working();
				frappe.call({
					method: "frappe.core.doctype.sms_settings.sms_settings.send_sms",
					args: {
						receiver_list: [v.number],
						msg: v.message
					},
					callback: function(r) {
						$(btn).done_working();
						if(r.exc) {frappe.msgprint(r.exc); return; }
						me.dialog.hide();
					}
				});
			}
		}
        this.dialog = d;
        this.setup_sms_template()
    };

    this.setup_sms_template = function() {
		var me = this;

		me.dialog.fields_dict["sms_template"].df.onchange = () => {
			var sms_template = me.dialog.fields_dict.sms_template.get_value();

			var prepend_reply = function(reply) {
				if(me.reply_added===sms_template) {
					return;
                }
                me.dialog.set_values({
                    'message': reply.message
                })
				me.reply_added = sms_template;
			}

			frappe.call({
				method: 'ticket_manage.sms_manage.doctype.sms_template.sms_template.get_sms_template',
				args: {
					template_name: sms_template,
					doc: doc
				},
				callback: function(r) {
					prepend_reply(r.message);
				},
			});
		}
	}
}