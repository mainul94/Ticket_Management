
/**
 * To Active this field system need to add select2 library in lib.min.js files in frappe app
 * #ToDo need to perfection this fieldtype value doesn't set perfactly.
 */

frappe.ui.form.ControlMultiLink = frappe.ui.form.ControlData.extend({
	html_element: "select",
	input_type: "text",
	get_options: function () {
		return this.df.options;
	},
	make_input: function(){
		this._super()
		this.setup_select2()
		console.log(this.get_value())
		this.refresh()
	},
	new_doc: function () {
		var doctype = this.get_options();
		var me = this;

		if (!doctype) return;

		// set values to fill in the new document
		if (this.df.get_route_options_for_new_doc) {
			frappe.route_options = this.df.get_route_options_for_new_doc(this);
		} else {
			frappe.route_options = {};
		}

		// partially entered name field
		// frappe.route_options.name_field = this.get_value();

		// reference to calling link
		frappe._from_link = this;
		frappe._from_link_scrollY = $(document).scrollTop();

		frappe.ui.form.make_quick_entry(doctype, (doc) => {
			return me.set_value(doc.name);
		});

		return false;
	},
	setup_select2: function () {
		let me = this;
		if (typeof me.df.faces_results === 'undefined'){
			me.df.faces_results = {}
		}
		let doctype = me.get_options();
		if (!doctype) return;
		let args = {
			'txt': '',
			'doctype': doctype,
			'ignore_user_permissions': me.df.ignore_user_permissions
		};
		this.$input.select2({
			initSelection: function(element, callback) {
				callback(me.get_selected_data());
			},
			ajax: {
				data: function (params) {
					args.txt = params.term
					return args
				},
				processResults: function (data) {
					$.map(data.results, function (d, i) {
						d['id'] = d.value
						d['text'] = d.description
						me.df.faces_results[d.id] = d.text
					})
					if (!me.df.only_select) {
						if (frappe.model.can_create(doctype)
							&& me.df.fieldtype !== "Dynamic Link") {
							// new item
							data.results.push({
								text: "<span class='text-primary link-option'>"
									+ "<i class='fa fa-plus' style='margin-right: 5px;'></i> "
									+ __("Create a new {0}", [__(me.df.options)])
									+ "</span>",
								id: "create_new__link_option",
								action: me.new_doc
							});
						}
					}
					return data
				},
				transport: function (params, success, failure) {
					var $request = frappe.call({
						type: "GET",
						method: 'frappe.desk.search.search_link',
						no_spinner: true,
						args: args
					});
					$request.then(success);
					$request.fail(failure);

					return $request;
				}
			},
			minimumInputLength: 1,
			templateResult: me.formatRepo,
			templateSelection: me.formatRepoSelection,
			cache: true
		});
		me.$input.on('select2:select select2:unselect', function(e){
			var item = e.params.data
			if(item.action) {
				item.value = "";
				item.action.apply(me);
			}
			// if remember_last_selected is checked in the doctype against the field,
			// then add this value
			// to defaults so you do not need to set it again
			// unless it is changed.
			if(me.df.remember_last_selected_value) {
				frappe.boot.user.last_selected_values[me.df.options] = item.value;
			}
			// me.$input.trigger('change');
		})
	},
	get_selected_data: function() {
		let list = []
		let values = this.value;
		if (typeof values !== "object")
			values = eval(values)
		for(let i = 0; typeof values !== 'undefined' && i <= values.length && typeof values[i] !== "undefined"; i++) {
			list.push({"id": values[i], "text": this.df.faces_results[values[i]] || values[i]})
		}
		return list
	},
	format_for_input: function(val) {
		if (typeof val !== "object")
			val = eval(val)
		return val==null ? "" : val;
	},
	formatRepo: function (repo) {
		if (repo.loading) {
			return repo.text;
		}

		var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__meta'>" +
			"<div class='select2-result-repository__title'><strong>" + (repo.id!=="create_new__link_option"? repo.id: '') + "</strong></div>";

		if (repo.text) {
			markup += "<div class='select2-result-repository__description'>" + repo.text + "</div>";
		}
		markup += '</div></div>'
		return $(markup);
	},
	formatRepoSelection: repo => {
		if (typeof repo.id ==='undefined' || repo.id.indexOf('__link_option') !== -1) {
			return false
		}
		return $(`<span><strong> ${(repo.id || repo.value)} </strong> ${(repo.text || repo.description)}</span>`)
	},
	set_custom_query: function (args) {
		var set_nulls = function (obj) {
			$.each(obj, function (key, value) {
				if (value !== undefined) {
					obj[key] = value;
				}
			});
			return obj;
		};
		if (this.get_query || this.df.get_query) {
			var get_query = this.get_query || this.df.get_query;
			if ($.isPlainObject(get_query)) {
				var filters = null;
				if (get_query.filters) {
					// passed as {'filters': {'key':'value'}}
					filters = get_query.filters;
				} else if (get_query.query) {

					// passed as {'query': 'path.to.method'}
					args.query = get_query;
				} else {

					// dict is filters
					filters = get_query;
				}

				if (filters) {
					filters = set_nulls(filters);

					// extend args for custom functions
					$.extend(args, filters);

					// add "filters" for standard query (search.py)
					args.filters = filters;
				}
			} else if (typeof (get_query) === "string") {
				args.query = get_query;
			} else {
				// get_query by function
				var q = (get_query)(this.frm && this.frm.doc || this.doc, this.doctype, this.docname);

				if (typeof (q) === "string") {
					// returns a string
					args.query = q;
				} else if ($.isPlainObject(q)) {
					// returns a plain object with filters
					if (q.filters) {
						set_nulls(q.filters);
					}

					// turn off value translation
					if (q.translate_values !== undefined) {
						this.translate_values = q.translate_values;
					}

					// extend args for custom functions
					$.extend(args, q);

					// add "filters" for standard query (search.py)
					args.filters = q.filters;
				}
			}
		}
		if (this.df.filters) {
			set_nulls(this.df.filters);
			if (!args.filters) args.filters = {};
			$.extend(args.filters, this.df.filters);
		}
	},
	set_input_attributes: function () {
		this._super()
		this.$input.attr('multiple', 'multiple')
	},
	validate: function(v) {
		return JSON.stringify(v)
	}
})