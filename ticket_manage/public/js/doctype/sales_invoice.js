// Copyright (c) 2018, Mainul Islam and contributors
// For license information, please see license.txt

frappe.provide("ticket.ticket");
ticket.ticket.InvoiceController = erpnext.accounts.SalesInvoiceController.extend({
	refresh: function (doc) {
		this._super(doc)
		this.set_reference_options()
	},
	set_reference_options: function() {
		frappe.meta.get_docfield("Ticket Item", "reference_device", this.frm.doc.name).options = [""].concat(this.get_reference_options());
	},
	get_reference_options: function() {
		if(typeof this.frm.doc.devices === 'undefined') {
			return []
		}
		let me = this;
		return this.frm.doc.devices.map(function(r){
			return me.get_reference(r)
		})
	},
	get_reference: function(device) {
		return device.device + (device.serial? '(' + device.serial + ')' : '')
	},
	validate: function(doc) {
		this.sync_device_items()
		this._super(doc)
	},
	sync_device_items: function(doc) {
		if(typeof doc === 'undefined') {
			doc = this.frm.doc
		}
		let me = this;
		let device_references = this.get_reference_options()
		let auto_created_tiems = doc.items.map(function(item){
			if(item.auto_created) {
				if (in_list(item.reference_device, device_references)) {
					return item.device_references
				} else {
					frappe.model.remove_from_locals(item.doctype, item.name)
				}
			}
		})
		$.each(doc.devices, function(i, device) {
			if(this.fault) {
				let new_val = this.fault;
				if (typeof new_val !== 'object') {
					new_val = eval(new_val)
				}
				$.each(new_val, function() {
					if (!in_list(me.get_reference(device), auto_created_tiems)) {
						let row = me.frm.add_child('items', {
							item_code: this,
							auto_created: 1,
							device_references: me.get_reference(device)
						})
						me.item_code(doc, row.doctype, row.name)
					}
				})
			}
		})
	}
});


$.extend(cur_frm.cscript,new ticket.ticket.InvoiceController({frm: cur_frm}))

