// Copyright (c) 2018, Mainul Islam and contributors
// For license information, please see license.txt

frappe.ui.form.on('Ticket Setting', {
	refresh: function(frm) {
		let buttons = {
			'item': __("Item"),
			'device': __("Device"),
			'customer': __("Customer")
		}
		$.each(buttons, (type_of_data, label) => {
			frm.add_custom_button(label, ()=>{
				frappe.call({
					method: 'pull_from_remote',
					doc:frm.doc,
					args: {
						type_of_data: type_of_data
					},
					freeze: true,
					freeze_message: __("Pulling..."),
					callback: r => {
						if(r['message'] && r.message==="Success")
							frappe.msgprint(__("Your request set in a queue"))
					}
				})
			}, __("Pull"))
		})
	}
});
