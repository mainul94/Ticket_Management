# -*- coding: utf-8 -*-
# Copyright (c) 2018, Mainul Islam and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import requests
from frappe.client import get_single_value
from frappe.utils.background_jobs import enqueue_doc


class TicketSetting(Document):
	def __init__(self, *args, **kwargs):
		super(TicketSetting, self).__init__(*args, **kwargs)
		self.map_fields = {
			'problems': {
				"target_doctype": "Item",
				"map_field_table":'prolemes_to_item',
				"default_values": {
					"item_group": get_single_value('Stock Settings', 'item_group'),
					"is_stock_item": 0
				}
			},
			'parts': {
				"target_doctype": "Item",
				"map_field_table":'parts_to_item',
				"default_values": {
					"item_group": get_single_value('Stock Settings', 'item_group'),
					"is_stock_item": 1
				}
			},
			'devices': {
				"target_doctype": "Device",
				"map_field_table":'devices_to_device'
			},
			'customers': {
				"target_doctype": "Customer",
				"map_field_table":'customers_to_customer',
				"default_values": {
					"customer_group": get_single_value('Stock Settings', 'customer_group'),
					"territory": get_single_value('Stock Settings', 'territory')
				},
				"pk": "cid"
			}

		}
		self.end_porint_map = {
			"item": ['parts', 'problems'],
			"device": "devices",
			"customer": "customers"
		}
		self._dict_doc = self.as_dict()
	def pull_from_remote(self, type_of_data=None):
		if not self.end_porint_map.get(type_of_data):
			return
		enqueue_doc(self.doctype, self.name, 'set_queue_doc_with_remote', type_of_data=type_of_data)
		return "Success"
	
	def set_queue_doc_with_remote(self, type_of_data):
		if isinstance(self.end_porint_map.get(type_of_data), list):
			for end_point in self.end_porint_map.get(type_of_data):
				self.send_request(end_point)
		else:
			self.send_request(self.end_porint_map.get(type_of_data))

	def send_request(self, end_point):
		data = self.get_apt_params()
		r = requests.get(self.get_api_url(end_point), data[0])
		if (r.status_code == 200):
			data = r.json().get('data')
			self.make_from_remote_data(end_point, data)

	def get_api_url(self, end_point):
		if self.url[-1] == '/' and end_point[0] == '/':
			return self.url + end_point[1:]
		elif self.url[-1] != '/' and end_point[0] != '/':
			return self.url + '/' + end_point[1:]
		else:
			return self.url + end_point

	def get_apt_params(self, end_point=None):
		data = {}
		header_data = {}
		for parm in self.parameters:
			if parm.header:
				header_data[parm.parameter] = parm.value
			else:
				data[parm.parameter] = parm.value
		return data, header_data

	def make_from_remote_data(self, end_point, data):
		mapped_fields = self.get_map_fields(end_point)
		doctype = self.map_fields.get(end_point).get('target_doctype')
		default_values = self.map_fields.get(end_point).get('default_values')
		pk_field = self.map_fields.get(end_point).get('pk') or "id"
		for d in data:
			if frappe.db.exists(doctype, {'repairdesk_id': d.get(pk_field)}):
				doc = frappe.db.exsist(doctype, {'repairdesk_id': d.get(pk_field)})
			else:
				doc = frappe.new_doc(doctype)
			if default_values:
				doc.update(default_values)
			if end_point == "customers":
				d['customer_group'] = d.get('customer_group').get('name')
			doc.update(d)
			for r_field, doc_field in mapped_fields.iteritems():
				doc.set(doc_field, d.get(r_field))

			if end_point == "devices":
				if doc.get('manufacturer') and not frappe.db.exists('Manufacturer', doc.get('manufacturer')):
					manufacturer = frappe.new_doc('Manufacturer')
					manufacturer.set('short_name', doc.get('manufacturer'))
					try:
						manufacturer.insert()
					except Exception as e:
						frappe.log_error(message=e, title="Manufacturer Isert Error")
			elif end_point == "customers":
				if doc.get('customer_group') and not frappe.db.exists('Customer Group', doc.get('customer_group')):
					cst = frappe.new_doc('Customer Group')
					cst.set('parent_customer_group','All Customer Groups')
					cst.set('customer_group_name', doc.get('customer_group'))
					try:
						cst.insert()
					except Exception as e:
						frappe.log_error(message=e, title="Customer Group Isert Error")
				if doc.get('territory') and not frappe.db.exists('Territory', doc.get('territory')):
					cst = frappe.new_doc('Territory')
					cst.set('parent_territory','All Territorys')
					cst.set('territory_name', doc.get('customer_group'))
					try:
						cst.insert()
					except Exception as e:
						frappe.log_error(message=e, title="Territory Isert Error")
				elif not doc.get('territory'):
					doc.set('territory', get_single_value('Stock Settings', 'territory'))

			if doc.is_new():
				try:
					doc.insert()
				except Exception as e:
					frappe.log_error(message=e, title="RepairDesk Sync Error")

	def get_map_fields(self, end_point):
		_map = {}
		table_fieldnaem = self.map_fields.get(end_point).get('map_field_table')
		if not self.get(table_fieldnaem):
			return _map
		for row in self.get(table_fieldnaem):
			_map[row.remote_field] = row.erp_field
		return _map