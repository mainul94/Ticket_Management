# -*- coding: utf-8 -*-
# Copyright (c) 2018, Mainul Islam and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.website.website_generator import WebsiteGenerator
from frappe.model.mapper import get_mapped_doc
from frappe.utils import random_string, get_url
import json
from frappe.model.db_schema import type_map, varchar_len
from six import string_types
from frappe.client import get_single_value

type_map['MultiLink'] = ('varchar', varchar_len)

class Ticket(WebsiteGenerator):
	def autoname(self):
		pass
	def validate(self):
		if not self.token:
			self.set_token()
		if not self.flags.from_doctype:
			self.flags.from_doctype = self.doctype
		if not self.status:
			self.set('status', get_single_value('Ticket Setting', 'default_status'))
	def set_token(self):
		key = random_string(32)
		self.db_set('token', key, False)
	
	def on_update(self):
		if self.flags.from_doctype == self.doctype:
			self.sync_invoice()

	def sync_invoice(self):
		if frappe.db.exists('Sales Invoice', {'ticket': self.name}):
			doc = frappe.get_doc('Sales Invoice', {'ticket': self.name})
		else:
			doc = frappe.new_doc('Sales Invoice')
		doc.flags.from_doctype = self.doctype
		doc = make_invoice(self.name, doc)
		doc.save()



@frappe.whitelist()
def make_invoice(source_name, target_doc=None):
	if target_doc:
		target_doc.set('devices', [])
		target_doc.set('items', [])
		target_doc.set('taxes', [])
	def postprocess(source, target):
		if not target.due_date:
			target.set('due_date', source.due_date)
	target_doc = get_mapped_doc("Ticket", source_name,
		{
			"Ticket": {
				"doctype": "Sales Invoice",
			},
			"Ticket Item": {
				"doctype": "Sales Invoice Item",
				# "field_map":[
				# 	['name', 'ticket_details'],
				# 	['parent', 'ticlet']
				# ]
			}
		}, target_doc, postprocess)
	return target_doc


@frappe.whitelist()
def make_service(source_name, target_doc=None):
	if not target_doc and frappe.db.exists('Service Report', {"ticket_details": source_name}):
		return frappe.get_doc('Service Report', {"ticket_details": source_name})
	def postprocess(source, target):
		if not target.branch:
			parent_doc = frappe.get_doc('Ticket', source.parent)
			parent_field_map = {
				"branch": "branch",
				"customer": "customer",
				"email": "email",
				"mobile": "mobile"
			}
			for s_field, t_field in parent_field_map.iteritems():
				if not target.get(t_field):
					target.set(t_field, parent_doc.get(s_field))
	target_doc = get_mapped_doc("Ticket Device", source_name,
		{
			"Ticket Device": {
				"doctype": "Service Report",
				"field_map": [
					['parent', 'ticket'],
					['name', 'ticket_details']
				]
			}
		}, target_doc, postprocess)
	return target_doc