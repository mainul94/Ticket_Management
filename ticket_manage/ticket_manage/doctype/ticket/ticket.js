// Copyright (c) 2018, Mainul Islam and contributors
// For license information, please see license.txt
{% include 'erpnext/selling/sales_common.js' %};


frappe.provide("ticket.ticket");
ticket.ticket.ticketController = erpnext.selling.SellingController.extend({
	setup: function (doc) {
		this._super(doc);
	},
	onload: function () {
		if (this.frm.doc.__islocal) {
			this.frm.set_value('status', '')
		}
	},
	refresh: function (doc) {
		this._super(doc)
		let me = this;
		if (!doc.__islocal) {
			this.frm.add_custom_button(__("Send SMS"), me.send_sms_to_customer)
			this.frm.add_custom_button(__("Send Email"), me.send_email)
			// if (!in_list(doc.status, ['Cancelled', 'Waiting on Customer'])) {
			// 	this.frm.add_custom_button(__("Invoice"), me.make_invoice, __("Make"))
			// 	this.frm.page.set_inner_btn_group_as_primary(__("Make"))
			// }
			frappe.call({
				method: 'frappe.client.get_value',
				args: {
					doctype: 'Sales Invoice',
					filters: {
						'ticket': me.frm.doc.name,
						'docstatus': ['!=', 2]
					},
					fieldname: 'name'
				},
				callback: r => {
					if (r['message']) {
						me.frm.add_custom_button(__("Invoice"), () => {
							frappe.set_route('Form', 'Sales Invoice', r.message.name)
						}, __("View"))
					}
				}
			})
			this.frm.page.set_inner_btn_group_as_primary(__("View"))
			this.frm.add_custom_button(__("Service Report"), () => {
				frappe.route_options = {
					"ticket": doc.name,
					"docstatus": ["!=", 2]
				}
				frappe.set_route('List', "Service Report")
			}, __("View"))
		}
		this.frm.add_custom_button(__("POS"), function () { frappe.set_route('pos') })
		this.set_reference_options()
		this.frm.set_df_property('start_work', 'hidden', this.frm.doc.__islocal, this.frm.doc.name, 'devices')
	},
	start_work: (doc, cdt, cdn) => {
		let local_doc = frappe.get_doc(cdt, cdn)
		// frappe.model.open_mapped_doc({
		// 	method: 'ticket_manage.ticket_manage.doctype.ticket.ticket.make_service',
		// 	doc: local_doc
		// })
		frappe.call({
			method: 'ticket_manage.ticket_manage.doctype.ticket.ticket.make_service',
			args: {
				source_name: cdn
			},
			callback: r => {
				if (!r.exe) {
					frappe.model.sync(r.message)
					frappe.set_route("Form", r.message.doctype, r.message.name);
				}
			}
		})
	},
	set_reference_options: function () {
		frappe.meta.get_docfield("Ticket Item", "reference_device", this.frm.doc.name).options = [""].concat(this.get_reference_options());
	},
	get_reference_options: function () {
		if (typeof this.frm.doc.devices === 'undefined') {
			return []
		}
		let me = this;
		return this.frm.doc.devices.map(function (r) {
			return me.get_reference(r)
		})
	},
	get_reference: function (device) {
		return device.device + (device.serial ? '(' + device.serial + ')' : '')
	},
	make_invoice: function () {
		frappe.model.open_mapped_doc({
			method: 'ticket_manage.ticket_manage.doctype.ticket.ticket.make_invoice',
			frm: me.frm
		})
	},
	send_sms_to_customer: () => {
		let doc = this.frm.doc;
		var sms_man = new ticket.SMSManager(doc);
		sms_man.show(doc.mobile, doc.doctype, doc.name, doc.mobile)
	},
	send_email: () => {
		let me = this;
		var args = {
			doc: me.frm.doc,
			frm: me.frm,
			recipients: me.frm.doc.email,
			txt: "",
			subject: ""
		}
		new frappe.views.CommunicationComposer(args)
	},
	validate: function (doc) {
		this.sync_device_items()
		this._super(doc)
	},
	sync_device_items: function (doc) {
		if (typeof doc === 'undefined') {
			doc = this.frm.doc
		}
		let me = this;
		let device_references = this.get_reference_options()
		let auto_created_tiems = doc.items.map(function (item) {
			if (item.auto_created) {
				if (in_list(item.reference_device, device_references)) {
					return item.device_references
				} else {
					frappe.model.remove_from_locals(item.doctype, item.name)
				}
			}
		})
		$.each(doc.devices, function (i, device) {
			if (this.fault) {
				let new_val = this.fault;
				if (typeof new_val !== 'object') {
					new_val = eval(new_val)
				}
				$.each(new_val, function () {
					if (!in_list(me.get_reference(device), auto_created_tiems)) {
						let row = me.frm.add_child('items', {
							item_code: this,
							auto_created: 1,
							device_references: me.get_reference(device)
						})
						me.item_code(doc, row.doctype, row.name)
					}
				})
			}
		})
	}
});


$.extend(cur_frm.cscript, new ticket.ticket.ticketController({ frm: cur_frm }))

