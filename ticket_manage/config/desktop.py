# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Ticket Manage",
			"color": "green",
			"icon": "octicon octicon-tools",
			"type": "module",
			"label": _("Ticket Manage")
		}
	]
