from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
            "label": _("Documents"),
			"items": [
				{
					"type": "doctype",
					"name": "Ticket",
				},
				{
					"type": "doctype",
					"name": "Service Report",
				},
				{
					"type": "doctype",
					"name": "Sales Invoice",
					"label": _("Invoice"),
				}
            ]
        },
        {
            "label": _("Setup"),
			"items": [
				{
					"type": "doctype",
					"name": "Ticket Setting",
				},
				{
					"type": "doctype",
					"name": "Ticket Status",
				},
				{
					"type": "doctype",
					"name": "Device",
				}
            ]
        }
    ]
