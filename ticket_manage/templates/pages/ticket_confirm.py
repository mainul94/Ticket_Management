import frappe

def get_context(context):
    context['message']= {
        "class": 'info',
        "title": 'Please !',
        "msg": "confirm you ticker"
    }
    context.update(frappe.form_dict)
    if context.get('ticket') and context.get('confirm'):
        try:
            doc = frappe.get_doc('Ticket', context.get('ticket'))
        except Exception as identifier:
            pass
        if not doc:
            return    
        status = frappe.db.get_single_value('Ticket Setting', context.get('confirm').lower() + '_status')
        doc.set('status', status)
        if doc.token == context.get('token'):
            doc.set_token()
            doc.save(ignore_permissions=True)
            _class = 'success' if context.get('confirm') == "Confirm" else 'danger'
            context['message']= {
                "class": _class,
                "title": 'Thanks !',
                "msg": "for {} you ticker.".format(context.get('confirm'))
            }
        else:
            context['message']= {
                "class": 'danger',
                "title": 'Error !',
                "msg": "Token doesn't match."
            }
    